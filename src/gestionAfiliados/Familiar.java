package gestionAfiliados;

public class Familiar extends Persona{

    public Familiar() {
    }

    public Familiar(String nombre, String dni, String telefono, String correoElectronico, Direccion domicilio) {
        super(nombre, dni, telefono, correoElectronico, domicilio);
    }
}
