package gestionAfiliados;

import excepciones.PersonaEncontrada;
import excepciones.PersonaNoEncontrada;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Afiliado extends Persona implements Comparable<Afiliado>{
    private String tipoDeCobertura;
    private String estadoDeCuenta;
    private String numeroAfiliado;
    private String cantFamiliares;
    private List<Familiar> familiares;

    public Afiliado(String nombre, String dni, String telefono, String correoElectronico, Direccion domicilio, String tipoDeCobertura, String estadoDeCuenta, String numeroAfiliado, String cantFamiliares) {
        super(nombre, dni, telefono, correoElectronico, domicilio);
        this.tipoDeCobertura = tipoDeCobertura;
        this.estadoDeCuenta = estadoDeCuenta;
        this.numeroAfiliado = numeroAfiliado;
        this.cantFamiliares = cantFamiliares;
        this.familiares = new ArrayList<Familiar>();
    }

    public String getCantFamiliares() {
        return cantFamiliares;
    }

    public void setCantFamiliares(String cantFamiliares) {
        this.cantFamiliares = cantFamiliares;
    }

    public String getNumeroAfiliado() {
        return numeroAfiliado;
    }

    public List<Familiar> getFamiliares() {
        return familiares;
    }
    
    public void agregarFamiliar(Familiar familiar) throws PersonaEncontrada{
        if(familiares.contains(familiar)){
            throw new PersonaEncontrada("El familiar que intenta agregar ya se encuentra registrado.");
        } else {
            familiares.add(familiar);
        }
   }
    
    public void eliminarFamiliar(Familiar familiarEliminar) throws PersonaNoEncontrada{
         if(familiares.contains(familiarEliminar)){
            familiares.remove(familiarEliminar);
        }else{
           throw new PersonaNoEncontrada("El familiar que intenta eliminar no se encuentra registrado.");
        }
    } 
    
    public void modificarFamiliar(Familiar familiarExistente, Familiar nuevoFamiliar) throws PersonaNoEncontrada{
        if(familiares.contains(familiarExistente)){
            int indice = familiares.indexOf(familiarExistente);
            familiares.set(indice, nuevoFamiliar);
          }
        else{
            try{
                agregarFamiliar(nuevoFamiliar);
            }catch(PersonaEncontrada e){
                System.out.println(e.getMessage());
            }  
            throw new PersonaNoEncontrada("El familiar que intenta modificar no se encuentra registrado, por lo tanto sera registrado.");
          }
    }
      
    public Familiar buscarFamiliar(String nombre, String dni) throws PersonaNoEncontrada{
        for(Familiar familiarAsociado: familiares){
            if(familiarAsociado.getNombre().equals(nombre) && familiarAsociado.getDni().equals(dni)){
                return familiarAsociado;
            }
        }
        throw new PersonaNoEncontrada("El familiar no se encuentra registrado.");
    }

    public String getTipoDeCobertura() {
        return tipoDeCobertura;
    }

    public String getEstadoDeCuenta() {
        return estadoDeCuenta;
    }

    public void setFamiliares(List<Familiar> familiares) {
        this.familiares = familiares;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Afiliado afiliadoAux = (Afiliado) obj;
        return Objects.equals(numeroAfiliado, afiliadoAux.numeroAfiliado) && Objects.equals(getDni(), afiliadoAux.getDni());
    }
    

    @Override
    public String toString() {
        return "Afiliado{" + "tipoDeCobertura=" + tipoDeCobertura + ", estadoDeCuenta=" + estadoDeCuenta + ", numeroAfiliado=" + numeroAfiliado + ", familiares=" + familiares + '}';
    }

    @Override
    public int compareTo(Afiliado otroAfiliado) {
        return this.getNombre().compareTo(otroAfiliado.getNombre());
    }
    
    
}
