package gestionAfiliados;

import excepciones.PersonaEncontrada;
import excepciones.PersonaNoEncontrada;
import java.util.List;
import java.util.ArrayList;

public class FicheroAfiliados{
    private List<Afiliado> listaAfiliados;
    
    public FicheroAfiliados(){
        this.listaAfiliados = new ArrayList<Afiliado>();
    }
    
    public void agregarAfiliado(Afiliado afiliado) throws PersonaEncontrada{
        if(listaAfiliados.contains(afiliado)){
            throw new PersonaEncontrada("El afiliado que intenta agregar ya se encuentra registrado.");
        }else {
            listaAfiliados.add(afiliado);
        }
    }

    public void eliminarAfiliado(String dni) throws PersonaNoEncontrada{
        Afiliado afiliadoEliminar = buscarAfiliado(dni);
        if(listaAfiliados.contains(afiliadoEliminar)){
            listaAfiliados.remove(afiliadoEliminar);
        }else{
            throw new PersonaNoEncontrada("El afiliado que intenta eliminar no se encuentra registrado.");
        }
    }

    public void modificarAfiliado(Afiliado afiliadoExistente, Afiliado nuevoAfiliado) throws PersonaNoEncontrada{
        if(listaAfiliados.contains(afiliadoExistente)){
            int indice = listaAfiliados.indexOf(afiliadoExistente);
            listaAfiliados.set(indice, nuevoAfiliado);
        }
        else{
            try{
                agregarAfiliado(nuevoAfiliado);
            }catch(PersonaEncontrada e){
                System.out.println(e.getMessage());
            }
            throw new PersonaNoEncontrada("\nEl afiliado que desea modificar no se encuentra registrado, por lo tanto sera registrado. ");
        }
    }

    public Afiliado buscarAfiliado(String dni) throws PersonaNoEncontrada {
       
        for (Afiliado afiliado : listaAfiliados) {
            if (afiliado.getDni().equals(dni)) {
                return afiliado;
            }
        }
        throw new PersonaNoEncontrada("No se encontro el afiliado buscado.");
    }
    public List<Afiliado> getListaAfiliados() {
        return listaAfiliados;
    }
    
}
