package gestionAfiliados;

public class Direccion {
    private String barrio;
    private String calle;
    private String numeroCasa;

    public Direccion(String barrio, String calle, String numeroCasa) {
        this.barrio = barrio;
        this.calle = calle;
        this.numeroCasa = numeroCasa;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(String numeroCasa) {
        this.numeroCasa = numeroCasa;
    }
    
    
}
