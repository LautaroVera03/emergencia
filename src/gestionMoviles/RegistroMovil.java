
package gestionMoviles;
import excepciones.MovilEncontrado;
import excepciones.MovilNoEncontrado;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class RegistroMovil {
    private List<Movil> listaMovil;
    
    public RegistroMovil(){
        this.listaMovil= new ArrayList<>();
    }
    
    public void agregarMovil(Movil movil)throws MovilEncontrado{
        if(listaMovil.contains(movil)){
            throw new MovilEncontrado("\nEl Movil que intenta agregar ya se encuentra registrado");
        }else {
            listaMovil.add(movil);
        }   
    }
    public void eliminarMovil(String patente) throws MovilNoEncontrado {
        Movil movilEliminar = buscarMovil(patente);
       if(listaMovil.contains(movilEliminar)){
            listaMovil.remove(movilEliminar);
        }else{
            throw new MovilNoEncontrado("El movil que intenta eliminar no se encuentra registrado.");
        }
    }
    public void modificarMovil(Movil movilExistente, Movil nuevoMovil) throws MovilNoEncontrado{
        if(listaMovil.contains(movilExistente)){
            int indice = listaMovil.indexOf(movilExistente);
            listaMovil.set(indice, nuevoMovil);
        }
        else{
            try{
                agregarMovil(nuevoMovil);
            }catch(MovilEncontrado e){
                System.out.println(e.getMessage());
            }
            throw new MovilNoEncontrado("\nEl movil que desea modificar no se encuentra registrado, por lo tanto sera registrado. ");
        }
    }
    
    public Movil buscarMovil(String numeroPatenteMovil) throws MovilNoEncontrado {
        for (Movil movil : listaMovil) {
            if (movil.getPatente().equals(numeroPatenteMovil)) {
                return movil;
            }
        }
        throw new MovilNoEncontrado("No se encontro el movil buscado.");
    }
    public List<Movil> getListaMovil() {
        return listaMovil;
    }
}
