
package gestionMoviles;

import java.time.LocalDate;

public class Movil implements Comparable<Movil> {
    private String marca;
    private String modelo;
    private String patente;
    private LocalDate anioFabricacion;
    private boolean mantenimientoRealizado;

    public Movil(String patente, String modelo, String marca, LocalDate anioFabricacion, Boolean mantenimientoRealizado) {
        this.patente = patente; 
        this.modelo = modelo;
        this.marca = marca;
        this.anioFabricacion = anioFabricacion;
        this.mantenimientoRealizado = mantenimientoRealizado;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public LocalDate getAnioFabricacion() {
        return anioFabricacion;
    }

    public void setAnioFabricacion(LocalDate anioFabricacion) {
        this.anioFabricacion = anioFabricacion;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movil other = (Movil) obj;
        return (this.patente == null ? other.patente == null : this.patente.equals(other.patente));
    }
    public boolean getMantenimientoRealizado() {
        return mantenimientoRealizado;
    }

    public void setMantenimientoRealizado(boolean mantenimientoRealizado) {
        this.mantenimientoRealizado = mantenimientoRealizado;
    }

    @Override
    public int compareTo(Movil otroMovil) {
    return this.patente.compareTo(otroMovil.patente);   
    }
    
     
 
}
