/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionMoviles;

import excepciones.MantenimientoRepetido;
import gestionMoviles.Movil;
import java.time.LocalDate;
import java.util.Objects;

public class Mantenimiento {
    private LocalDate fechaDeMantenimiento;
    private String tipoDemantenimiento;
    private Double costo;
    private Movil movilRelacionado;

    public Mantenimiento(LocalDate fechaDeMantenimiento, String tipoDemantenimiento, Double costo, Movil movilRelacionado) {
        this.fechaDeMantenimiento = fechaDeMantenimiento;
        this.tipoDemantenimiento = tipoDemantenimiento;
        this.costo = costo;
        this.movilRelacionado = movilRelacionado;
    }

    public LocalDate getFechaDeMantenimiento() {
        return fechaDeMantenimiento;
    }

    public void setFechaDeMantenimiento(LocalDate fechaDeMantenimiento) {
        this.fechaDeMantenimiento = fechaDeMantenimiento;
    }

    public String getTipoDemantenimiento() {
        return tipoDemantenimiento;
    }

    public void setTipoDemantenimiento(String tipoDemantenimiento) {
        this.tipoDemantenimiento = tipoDemantenimiento;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public Movil getMovilRelacionado() {
        return movilRelacionado;
    }

    public void setMovilRelacionado(Movil movilRelacionado) {
        this.movilRelacionado = movilRelacionado;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.fechaDeMantenimiento);
        hash = 53 * hash + Objects.hashCode(this.movilRelacionado);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mantenimiento other = (Mantenimiento) obj;
        if (!Objects.equals(this.fechaDeMantenimiento, other.fechaDeMantenimiento)) {
            return false;
        }
        return Objects.equals(this.movilRelacionado, other.movilRelacionado);
    }
    }
