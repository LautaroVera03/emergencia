package excepciones;

public class ItemNoEncontrado extends Exception {

    public ItemNoEncontrado(String msg){
        super(msg);
    }
}
