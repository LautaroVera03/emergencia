
package excepciones;


public class PersonaNoEncontrada extends Exception {
    public PersonaNoEncontrada(String mensaje) {
        super(mensaje);
    } 
}
