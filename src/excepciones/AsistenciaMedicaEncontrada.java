package excepciones;

public class AsistenciaMedicaEncontrada extends Exception{

    public AsistenciaMedicaEncontrada(String mensaje) {
        super(mensaje);
    }
}
