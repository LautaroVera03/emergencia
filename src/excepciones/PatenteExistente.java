
package excepciones;

public class PatenteExistente extends Exception  {
    public PatenteExistente(String patente) {
        super("La patente '" + patente + "' ya existe en la lista.");
    }
    
}
