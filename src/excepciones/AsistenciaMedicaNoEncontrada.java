package excepciones;

public class AsistenciaMedicaNoEncontrada extends Exception{

    public AsistenciaMedicaNoEncontrada(String mensaje) {
        super(mensaje);
    }
}
