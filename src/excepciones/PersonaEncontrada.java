package excepciones;

public class PersonaEncontrada extends Exception {
    
    public PersonaEncontrada(String mensaje) {
        super(mensaje);
    }
}
