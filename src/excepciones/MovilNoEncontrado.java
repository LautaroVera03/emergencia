
package excepciones;

public class MovilNoEncontrado extends Exception {
    public MovilNoEncontrado(String mensaje) {
        super(mensaje);
    } 
}
