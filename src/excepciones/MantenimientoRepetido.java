
package excepciones;

public class MantenimientoRepetido extends Exception {
    public MantenimientoRepetido(String message) {
        super(message);
    }
    
}
