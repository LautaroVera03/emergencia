
package Ventanas;

import emergencia.EmpresaEmergencia;
import excepciones.PersonaNoEncontrada;
import gestionAfiliados.Afiliado;
import gestionDePagos.Factura;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.util.List;

public class FacturaEleccion extends javax.swing.JFrame {
    private VentanaFactura ventanaFactura;
    private VerFactura verFactura;

    public FacturaEleccion(java.awt.Frame parent, boolean modal) {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btVerFacturas = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        btVerFacturas.setText("Ver facturas de Afiliados");
        btVerFacturas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVerFacturasActionPerformed(evt);
            }
        });

        btnAgregar.setText("Agregar factura");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btVerFacturas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(btVerFacturas)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(btnAgregar)
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
       String dniAfiliado = JOptionPane.showInputDialog(this, "Ingrese el DNI del afiliado:");

        if (dniAfiliado != null && !dniAfiliado.isEmpty()) {
            try {
                Afiliado afiliado = EmpresaEmergencia.instancia().getFicheroAfiliados().buscarAfiliado(dniAfiliado);
                VentanaFactura ventanaFactura = new VentanaFactura(this, true, afiliado);
                ventanaFactura.setVisible(true);
            } catch (PersonaNoEncontrada ex) {
                Logger.getLogger(FacturaEleccion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
        JOptionPane.showMessageDialog(this, "Debe ingresar un DNI válido.", "Error", JOptionPane.ERROR_MESSAGE);
    }
        dispose(); 
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btVerFacturasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVerFacturasActionPerformed
    String dniAfiliado = JOptionPane.showInputDialog(this, "Ingrese el DNI del afiliado:");

        if (dniAfiliado != null && !dniAfiliado.isEmpty()) {
            try {
                Afiliado afiliado = EmpresaEmergencia.instancia().getFicheroAfiliados().buscarAfiliado(dniAfiliado);
                VerFactura verFactura = new VerFactura(this, true, afiliado);
            } catch (PersonaNoEncontrada ex) {
                Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debe ingresar un DNI válido.", "Error", JOptionPane.ERROR_MESSAGE);
        }
        dispose();
    }//GEN-LAST:event_btVerFacturasActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btVerFacturas;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
