
package Ventanas;

import emergencia.EmpresaEmergencia;
import excepciones.PersonaEncontrada;
import excepciones.PersonaNoEncontrada;
import gestionAfiliados.Afiliado;
import gestionAfiliados.Direccion;
import gestionAfiliados.Familiar;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class AgregarAfiliado extends javax.swing.JDialog {
    private BuscarAfiliado ventanaBuscar;
    private EliminarAfiliado ventanaEliminar;
    private AgregarAfiliado ventanaDeAfiliado;
    private AgregarFamiliares ventanaFamilia;
    private Afiliado afiliado;
    private Direccion direccion;
   
    
    public AgregarAfiliado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Agregar Afiliado.");
        
        ventanaEliminar = new EliminarAfiliado(parent, true);
        ventanaBuscar = new BuscarAfiliado(parent, true);
        ventanaFamilia = new AgregarFamiliares(parent, true);
        agregarAfiliadoATabla();
        btnFamiliares.setEnabled(false);
        txtNombreAfiliado.setText("");
        txtDNIAfiliado.setText("");
        txtTelefonoAfiliado.setText("");
        txtCorreoAfiliado.setText("");
        txtBarrioAfiliado.setText("");
        txtCalleAfiliado.setText("");
        txtNumeroCasaAfiliado.setText("");
        txtNumeroDeAfiliado.setText("");
        btnTipoCoverturaAfiliado.insertItemAt(" ", 0);
        btnTipoCoverturaAfiliado.setSelectedItem(" ");
        btnEstadoCuentaAfiliado.insertItemAt(" ", 0);
        btnEstadoCuentaAfiliado.setSelectedItem(" ");
        txtCantFamiliares.setText("");
        txtCantFamiliares.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            try {
                int cantidadFamiliares = Integer.parseInt(txtCantFamiliares.getText());
                btnFamiliares.setEnabled(cantidadFamiliares > 0);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(AgregarAfiliado.this, "Por favor, ingrese un número válido para la cantidad de familiares.", "Error", JOptionPane.ERROR_MESSAGE);
                btnFamiliares.setEnabled(false);
            }
        }
        });
       
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel16 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel18 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel19 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        jLabel22 = new javax.swing.JLabel();
        jSeparator13 = new javax.swing.JSeparator();
        jSeparator14 = new javax.swing.JSeparator();
        jLabel23 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnAgregarAfiliado = new javax.swing.JButton();
        btnLimpiarCasillas = new javax.swing.JButton();
        btnEliminarAfiliado = new javax.swing.JButton();
        btnVolverAtras = new javax.swing.JButton();
        btnBuscarAfiliado = new javax.swing.JButton();
        btnModificarAfiliado = new javax.swing.JButton();
        btnEliminarFamiliar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTablaDeRegistrosDeAfiliado = new javax.swing.JTable();
        btnEstadoCuentaAfiliado = new javax.swing.JComboBox<>();
        btnTipoCoverturaAfiliado = new javax.swing.JComboBox<>();
        btnFamiliares = new javax.swing.JButton();
        txtNombreAfiliado = new javax.swing.JFormattedTextField();
        txtTelefonoAfiliado = new javax.swing.JFormattedTextField();
        txtDNIAfiliado = new javax.swing.JFormattedTextField();
        txtCorreoAfiliado = new javax.swing.JFormattedTextField();
        txtCantFamiliares = new javax.swing.JFormattedTextField();
        txtNumeroDeAfiliado = new javax.swing.JFormattedTextField();
        txtBarrioAfiliado = new javax.swing.JFormattedTextField();
        txtCalleAfiliado = new javax.swing.JFormattedTextField();
        txtNumeroCasaAfiliado = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setForeground(new java.awt.Color(0, 102, 102));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nombre:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        jLabel6.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Domicilio:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 120, 67, -1));

        jLabel9.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Numero de afiliado:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, -1));

        jLabel11.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Cant. de familiares:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 260, -1, -1));

        jLabel10.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Barrio:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 150, -1, -1));

        jLabel12.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Calle:");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 180, 40, -1));

        jLabel14.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("N° de casa:");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 210, -1, -1));

        jPanel2.setBackground(new java.awt.Color(102, 102, 102));

        jLabel1.setFont(new java.awt.Font("Roboto", 1, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("REGISTRO DE AFILIADOS");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(151, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(140, 140, 140))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 890, -1));

        jLabel15.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Datos del afiliado:");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, -1));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 120, 10));

        jLabel16.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Documento:");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 230, 70, 10));

        jLabel18.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Telefono:");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 120, -1, -1));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 140, 210, 10));

        jLabel19.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Correo Electronico:");
        jPanel1.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 190, -1, -1));

        jSeparator5.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 320, 210, 10));

        jSeparator6.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 210, 10));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 140, 60, 10));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 200, 40, 10));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 170, 40, 10));

        jSeparator11.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 210, 10));

        jSeparator12.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 320, 210, 10));

        jLabel22.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Tipo de cobertura:");
        jPanel1.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 300, -1, -1));

        jSeparator13.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator13, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 210, 210, 10));

        jSeparator14.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator14, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 320, 210, 10));

        jLabel23.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Estado de cuenta:");
        jPanel1.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 300, -1, -1));

        jPanel3.setBackground(new java.awt.Color(102, 102, 102));

        btnAgregarAfiliado.setBackground(new java.awt.Color(51, 51, 51));
        btnAgregarAfiliado.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btnAgregarAfiliado.setForeground(new java.awt.Color(255, 255, 255));
        btnAgregarAfiliado.setText("Guardar");
        btnAgregarAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarAfiliadoActionPerformed(evt);
            }
        });

        btnLimpiarCasillas.setBackground(new java.awt.Color(51, 51, 51));
        btnLimpiarCasillas.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btnLimpiarCasillas.setForeground(new java.awt.Color(255, 255, 255));
        btnLimpiarCasillas.setText("Limpiar");
        btnLimpiarCasillas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarCasillasActionPerformed(evt);
            }
        });

        btnEliminarAfiliado.setBackground(new java.awt.Color(51, 51, 51));
        btnEliminarAfiliado.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btnEliminarAfiliado.setForeground(new java.awt.Color(255, 255, 255));
        btnEliminarAfiliado.setText("Eliminar");
        btnEliminarAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAfiliadoActionPerformed(evt);
            }
        });

        btnVolverAtras.setBackground(new java.awt.Color(51, 51, 51));
        btnVolverAtras.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btnVolverAtras.setForeground(new java.awt.Color(255, 255, 255));
        btnVolverAtras.setText("Atras");
        btnVolverAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverAtrasActionPerformed(evt);
            }
        });

        btnBuscarAfiliado.setBackground(new java.awt.Color(51, 51, 51));
        btnBuscarAfiliado.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btnBuscarAfiliado.setForeground(new java.awt.Color(255, 255, 255));
        btnBuscarAfiliado.setText("Buscar");
        btnBuscarAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarAfiliadoActionPerformed(evt);
            }
        });

        btnModificarAfiliado.setBackground(new java.awt.Color(51, 51, 51));
        btnModificarAfiliado.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btnModificarAfiliado.setForeground(new java.awt.Color(255, 255, 255));
        btnModificarAfiliado.setText("Modificar");
        btnModificarAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarAfiliadoActionPerformed(evt);
            }
        });

        btnEliminarFamiliar.setBackground(new java.awt.Color(51, 51, 51));
        btnEliminarFamiliar.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btnEliminarFamiliar.setForeground(new java.awt.Color(255, 255, 255));
        btnEliminarFamiliar.setText("Eliminar Familiar");
        btnEliminarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarFamiliarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(btnAgregarAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLimpiarCasillas, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(btnModificarAfiliado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuscarAfiliado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminarAfiliado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEliminarFamiliar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                .addComponent(btnVolverAtras, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLimpiarCasillas, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVolverAtras, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModificarAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 620, 890, 60));

        JTablaDeRegistrosDeAfiliado.setBackground(new java.awt.Color(255, 255, 255));
        JTablaDeRegistrosDeAfiliado.setForeground(new java.awt.Color(0, 0, 0));
        JTablaDeRegistrosDeAfiliado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(JTablaDeRegistrosDeAfiliado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 360, 890, 260));

        btnEstadoCuentaAfiliado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Activo", "Inactivo" }));
        btnEstadoCuentaAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstadoCuentaAfiliadoActionPerformed(evt);
            }
        });
        jPanel1.add(btnEstadoCuentaAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 330, 210, -1));

        btnTipoCoverturaAfiliado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básica", "Premium" }));
        jPanel1.add(btnTipoCoverturaAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 330, 210, -1));

        btnFamiliares.setBackground(new java.awt.Color(51, 51, 51));
        btnFamiliares.setForeground(new java.awt.Color(255, 255, 255));
        btnFamiliares.setText("Agregar Familiares");
        btnFamiliares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFamiliaresActionPerformed(evt);
            }
        });
        jPanel1.add(btnFamiliares, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 260, 160, -1));

        try {
            txtNombreAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("******************************")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtNombreAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreAfiliadoActionPerformed(evt);
            }
        });
        jPanel1.add(txtNombreAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 210, -1));

        try {
            txtTelefonoAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-#######")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtTelefonoAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelefonoAfiliadoActionPerformed(evt);
            }
        });
        jPanel1.add(txtTelefonoAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 150, 210, -1));

        try {
            txtDNIAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtDNIAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 210, -1));

        try {
            txtCorreoAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("****************************************")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtCorreoAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 220, 210, -1));

        txtCantFamiliares.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txtCantFamiliares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantFamiliaresActionPerformed(evt);
            }
        });
        jPanel1.add(txtCantFamiliares, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 260, 130, -1));

        txtNumeroDeAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jPanel1.add(txtNumeroDeAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 210, -1));

        try {
            txtBarrioAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("****************************************")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtBarrioAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 150, 220, -1));

        try {
            txtCalleAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("******************************")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtCalleAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 180, 220, -1));

        txtNumeroCasaAfiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jPanel1.add(txtNumeroCasaAfiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 210, 220, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 890, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 681, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 681, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

        
    private void btnAgregarAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarAfiliadoActionPerformed

        String nombre = txtNombreAfiliado.getText();
        String dni = txtDNIAfiliado.getText();
        String telefono = txtTelefonoAfiliado.getText();
        String correo = txtCorreoAfiliado.getText();
        String barrio = txtBarrioAfiliado.getText();
        String calle = txtCalleAfiliado.getText();
        String numeroCasa = txtNumeroCasaAfiliado.getText();
        String cobertura = (String) btnTipoCoverturaAfiliado.getSelectedItem();
        String estadoCuenta = (String) btnEstadoCuentaAfiliado.getSelectedItem() ;
        String numero = txtNumeroDeAfiliado.getText();
        String cantFamiliares = txtCantFamiliares.getText();

        if (nombre.isEmpty() || dni.isEmpty() || telefono.isEmpty() || correo.isEmpty() ||
            barrio.isEmpty() || calle.isEmpty() ||  numeroCasa.isEmpty() ||
            cobertura == " " || estadoCuenta == " " || numero.isEmpty() || cantFamiliares.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Por favor, complete todos los campos.", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            Direccion direccion = new Direccion(barrio, calle, numeroCasa);

            try {
                int cantidadFamiliares = Integer.parseInt(cantFamiliares);
                
                if(cantidadFamiliares < 0) {
                JOptionPane.showMessageDialog(this, "Por favor, ingrese una cantidad válida de familiares.", "Advertencia", JOptionPane.WARNING_MESSAGE);
            } else{
                Afiliado afiliado = new Afiliado(nombre, dni, telefono, correo, direccion, cobertura, estadoCuenta, numero, cantFamiliares);
                afiliado.setFamiliares(devolverListaFamiliar());
                EmpresaEmergencia.instancia().getFicheroAfiliados().agregarAfiliado(afiliado);
                agregarAfiliadoATabla();
                JOptionPane.showMessageDialog(this, "Se agregó correctamente el afiliado.");
                txtNombreAfiliado.setText("");
                txtDNIAfiliado.setText("");
                txtTelefonoAfiliado.setText("");
                txtCorreoAfiliado.setText("");
                txtBarrioAfiliado.setText("");
                txtCalleAfiliado.setText("");
                txtNumeroCasaAfiliado.setText("");
                txtNumeroDeAfiliado.setText("");
                btnTipoCoverturaAfiliado.setSelectedItem(" ");
                btnEstadoCuentaAfiliado.setSelectedItem(" ");
                txtCantFamiliares.setText("");
            }} catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Por favor, ingrese una cantidad válida de familiares.", "Advertencia", JOptionPane.WARNING_MESSAGE);
        } catch (PersonaEncontrada e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
    btnFamiliares.setEnabled(true);

    }//GEN-LAST:event_btnAgregarAfiliadoActionPerformed

    private void btnVolverAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverAtrasActionPerformed
       this.setVisible(false);
    }//GEN-LAST:event_btnVolverAtrasActionPerformed

    private void btnLimpiarCasillasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarCasillasActionPerformed
       txtNombreAfiliado.setText("");
       txtDNIAfiliado.setText("");
       txtTelefonoAfiliado.setText("");
       txtCorreoAfiliado.setText("");
       btnTipoCoverturaAfiliado.setSelectedItem(" ");
       btnEstadoCuentaAfiliado.setSelectedItem(" ");
       txtBarrioAfiliado.setText("");
       txtCalleAfiliado.setText("");
       txtNumeroCasaAfiliado.setText("");
       txtNumeroDeAfiliado.setText("");
       txtCantFamiliares.setText("");
       ventanaBuscar.limpiarCasillaDNI();
    }//GEN-LAST:event_btnLimpiarCasillasActionPerformed

    private void btnEliminarAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAfiliadoActionPerformed
        ventanaEliminar.setVisible(true);

          if (ventanaEliminar.isConfirmado()) {
              String dni = ventanaEliminar.getDniEliminar();
              if (dni.isEmpty()) {
                  JOptionPane.showMessageDialog(this, "Por favor, ingrese un DNI válido para eliminar al afiliado.", "Error", JOptionPane.ERROR_MESSAGE);
                  ventanaEliminar.limpiarCasillaDNI();
                  ventanaEliminar.setVisible(false);
                  ventanaBuscar.limpiarCasillaDNI();
              } else {
                  try {
                      EmpresaEmergencia.instancia().getFicheroAfiliados().eliminarAfiliado(dni);
                      JOptionPane.showMessageDialog(this, "Se eliminó correctamente el afiliado.");
                      agregarAfiliadoATabla();
                  } catch (PersonaNoEncontrada e) {
                      JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                      ventanaBuscar.limpiarCasillaDNI();
                  }
              }
          } else {
              JOptionPane.showMessageDialog(this, "Se canceló la eliminación del afiliado.");
              ventanaBuscar.limpiarCasillaDNI();
          }
          ventanaBuscar.limpiarCasillaDNI();
          ventanaEliminar.setVisible(false); 

    }//GEN-LAST:event_btnEliminarAfiliadoActionPerformed

    private void btnBuscarAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarAfiliadoActionPerformed
        ventanaBuscar.setVisible(true);
        
        if(ventanaBuscar.isConfirmar()){
            String dni = ventanaBuscar.getDniBuscar();
            try {
                Afiliado afiliado = EmpresaEmergencia.instancia().getFicheroAfiliados().buscarAfiliado(dni);
                mostrarInformacionAfiliado(afiliado);
            } catch (PersonaNoEncontrada e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                ventanaBuscar.limpiarCasillaDNI();
            }
        }else{
            JOptionPane.showMessageDialog(this, "Se cancelo la busqueda del afiliado");
        }
    }//GEN-LAST:event_btnBuscarAfiliadoActionPerformed

    private void btnModificarAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarAfiliadoActionPerformed
        ventanaBuscar.setVisible(true);
        if(ventanaBuscar.isConfirmar()){
            String dni = ventanaBuscar.getDniBuscar();
            if(dni.isEmpty()){
            JOptionPane.showMessageDialog(this, "Por favor, ingrese un DNI válido para buscar y modificar al afiliado.", "Error", JOptionPane.ERROR_MESSAGE);
            ventanaBuscar.limpiarCasillaDNI();
            ventanaBuscar.setVisible(true);
            }else{
            try {
            Afiliado afiliadoViejo = EmpresaEmergencia.instancia().getFicheroAfiliados().buscarAfiliado(dni);
            ventanaBuscar.setVisible(false);
            ventanaBuscar.limpiarCasillaDNI();
            txtNombreAfiliado.setText(afiliadoViejo.getNombre());
            txtDNIAfiliado.setText(afiliadoViejo.getDni());
            txtTelefonoAfiliado.setText(afiliadoViejo.getTelefono());
            txtCorreoAfiliado.setText(afiliadoViejo.getCorreoElectronico());
            txtBarrioAfiliado.setText(afiliadoViejo.getDomicilio().getBarrio());
            txtCalleAfiliado.setText(afiliadoViejo.getDomicilio().getCalle());
            txtNumeroCasaAfiliado.setText(afiliadoViejo.getDomicilio().getNumeroCasa());
            btnTipoCoverturaAfiliado.setSelectedItem(afiliadoViejo.getTipoDeCobertura());
            btnEstadoCuentaAfiliado.setSelectedItem(afiliadoViejo.getEstadoDeCuenta());
            txtNumeroDeAfiliado.setText(afiliadoViejo.getNumeroAfiliado());
            txtCantFamiliares.setText(afiliadoViejo.getCantFamiliares());
            
            EmpresaEmergencia.instancia().getFicheroAfiliados().eliminarAfiliado(dni);
            
        } catch (PersonaNoEncontrada e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
             }
            }
        }else{
            JOptionPane.showMessageDialog(this, "Se canceló la modificación del afiliado.");
            ventanaBuscar.limpiarCasillaDNI();
            ventanaBuscar.setVisible(false);
        }
        
    }//GEN-LAST:event_btnModificarAfiliadoActionPerformed

    private void btnEstadoCuentaAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstadoCuentaAfiliadoActionPerformed
    
    }//GEN-LAST:event_btnEstadoCuentaAfiliadoActionPerformed

    private void btnFamiliaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFamiliaresActionPerformed
        int cantFamiliares = Integer.parseInt(txtCantFamiliares.getText());
        if(cantFamiliares > 0 ){
        ventanaFamilia.setCantFamiliares(cantFamiliares);
        ventanaFamilia.setVisible(true);
        }
    }//GEN-LAST:event_btnFamiliaresActionPerformed

    private void btnEliminarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarFamiliarActionPerformed
        String dniAfiliado = JOptionPane.showInputDialog(this, "Ingrese el DNI del afiliado:");
    
    try {
        Afiliado afiliado = EmpresaEmergencia.instancia().getFicheroAfiliados().buscarAfiliado(dniAfiliado);
        List<Familiar> familiares = afiliado.getFamiliares();

        if (familiares.isEmpty()) {
            JOptionPane.showMessageDialog(this, "El afiliado no tiene familiares registrados.", "Información", JOptionPane.INFORMATION_MESSAGE);
        } else {
            StringBuilder mensaje = new StringBuilder("Familiares del afiliado:\n");

            for (int i = 0; i < familiares.size(); i++) {
                mensaje.append(i + 1).append(". ").append(familiares.get(i).getNombre()).append("\n");
            }

            int opcion = JOptionPane.showConfirmDialog(this, mensaje.toString() + "¿Desea eliminar algún familiar?", "Familiares del Afiliado", JOptionPane.YES_NO_OPTION);
            
            if (opcion == JOptionPane.YES_OPTION) {
                int numeroFamiliarEliminar = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el número del familiar que desea eliminar:")) - 1;
                
                if (numeroFamiliarEliminar >= 0 && numeroFamiliarEliminar < familiares.size()) {
                    Familiar familiarEliminar = familiares.get(numeroFamiliarEliminar);
                    afiliado.eliminarFamiliar(familiarEliminar);
                    JOptionPane.showMessageDialog(this, "Familiar eliminado correctamente.");
                } else {
                    JOptionPane.showMessageDialog(this, "Número de familiar inválido.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    } catch (PersonaNoEncontrada e) {
        JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
    }//GEN-LAST:event_btnEliminarFamiliarActionPerformed

    private void txtNombreAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreAfiliadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreAfiliadoActionPerformed

    private void txtTelefonoAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelefonoAfiliadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTelefonoAfiliadoActionPerformed

    private void txtCantFamiliaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantFamiliaresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantFamiliaresActionPerformed
    
    private void mostrarInformacionAfiliado(Afiliado afiliado) {
    StringBuilder mensaje = new StringBuilder();
    mensaje.append("Apellido y Nombre: ").append(afiliado.getNombre()).append("\n");
    mensaje.append("Documento: ").append(afiliado.getDni()).append("\n");
    mensaje.append("Teléfono: ").append(afiliado.getTelefono()).append("\n");
    mensaje.append("Correo Electrónico: ").append(afiliado.getCorreoElectronico()).append("\n");
    mensaje.append("Dirección:\n");
    mensaje.append("Barrio: ").append(afiliado.getDomicilio().getBarrio()).append(" Calle: ").append(afiliado.getDomicilio().getCalle()).append(" N° Casa: ").append(afiliado.getDomicilio().getNumeroCasa()).append("\n");
    mensaje.append("Número de Afiliado: ").append(afiliado.getNumeroAfiliado()).append("\n");
    mensaje.append("Tipo de Cobertura: ").append(afiliado.getTipoDeCobertura()).append("\n");
    mensaje.append("Cuenta del Afiliado: ").append(afiliado.getEstadoDeCuenta()).append("\n");
    mensaje.append("Cantidad de Familiares Agregados: ").append(afiliado.getCantFamiliares()).append("\n");

    Object[] options = {" Aceptar ", " Ver Familiares "};
    int choice = JOptionPane.showOptionDialog(this, mensaje.toString(), "Información del Afiliado",
            JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

    if (choice == 1) {
        mostrarInformacionFamiliares(afiliado.getFamiliares());
    }
}
    private void mostrarInformacionFamiliares(List<Familiar> familiares) {
    StringBuilder mensaje = new StringBuilder("Familiares del Afiliado:\n");

    for (Familiar familiar : familiares) {
        mensaje.append("\nApellido y Nombre: ").append(familiar.getNombre()).append("\nDocumento: ").append(familiar.getDni()).append("\nTelefono: ").append(familiar.getTelefono()).append("\n"
                + "Correo Electronico: ").append(familiar.getCorreoElectronico()).append("\n Direccion: ").append("\nBarrio: ").append(familiar.getDomicilio().getBarrio())
                .append(", Calle: ").append(familiar.getDomicilio().getCalle()).append(", N° Casa: ").append(familiar.getDomicilio().getNumeroCasa());
    }

    JOptionPane.showMessageDialog(this, mensaje.toString(), "Familiares del Afiliado",
            JOptionPane.INFORMATION_MESSAGE);
}          
    public List<Familiar> devolverListaFamiliar(){
        List<Familiar> familiares = ventanaFamilia.getFamiliares();
        return familiares;
    }    
    public List<Afiliado> obtenerAfiliadosOrdenadosPorApellido() {
    List<Afiliado> listaAfiliados = EmpresaEmergencia.instancia().getFicheroAfiliados().getListaAfiliados();
    Collections.sort(listaAfiliados);
    return listaAfiliados;
    }
    public void agregarAfiliadoATabla() {
    DefaultTableModel modelo = new DefaultTableModel();
    modelo.addColumn("Nombre Y Apellido");
    modelo.addColumn("DNI");
    modelo.addColumn("Telefono");
    modelo.addColumn("Barrio");
    modelo.addColumn("Calle");
    modelo.addColumn("N°Casa");
    modelo.addColumn("Cobertura");
    modelo.addColumn("EstadoCuenta");
    modelo.addColumn("N°Afiliado");
    List<Afiliado> afiliadosOrdenados = obtenerAfiliadosOrdenadosPorApellido();
    
    for (Afiliado afiliado : afiliadosOrdenados) {
        Object[] rowData = {
            afiliado.getNombre(),
            afiliado.getDni(),
            afiliado.getTelefono(),
            afiliado.getDomicilio().getBarrio(),
            afiliado.getDomicilio().getCalle(),
            afiliado.getDomicilio().getNumeroCasa(),
            afiliado.getTipoDeCobertura(),
            afiliado.getEstadoDeCuenta(),
            afiliado.getNumeroAfiliado()
        };
     modelo.addRow(rowData);
    } 
    JTablaDeRegistrosDeAfiliado.setModel(modelo);
    JTablaDeRegistrosDeAfiliado.setEnabled(false);
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable JTablaDeRegistrosDeAfiliado;
    private javax.swing.JButton btnAgregarAfiliado;
    private javax.swing.JButton btnBuscarAfiliado;
    private javax.swing.JButton btnEliminarAfiliado;
    private javax.swing.JButton btnEliminarFamiliar;
    private javax.swing.JComboBox<String> btnEstadoCuentaAfiliado;
    private javax.swing.JButton btnFamiliares;
    private javax.swing.JButton btnLimpiarCasillas;
    private javax.swing.JButton btnModificarAfiliado;
    private javax.swing.JComboBox<String> btnTipoCoverturaAfiliado;
    private javax.swing.JButton btnVolverAtras;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JFormattedTextField txtBarrioAfiliado;
    private javax.swing.JFormattedTextField txtCalleAfiliado;
    private javax.swing.JFormattedTextField txtCantFamiliares;
    private javax.swing.JFormattedTextField txtCorreoAfiliado;
    private javax.swing.JFormattedTextField txtDNIAfiliado;
    private javax.swing.JFormattedTextField txtNombreAfiliado;
    private javax.swing.JFormattedTextField txtNumeroCasaAfiliado;
    private javax.swing.JFormattedTextField txtNumeroDeAfiliado;
    private javax.swing.JFormattedTextField txtTelefonoAfiliado;
    // End of variables declaration//GEN-END:variables
}
