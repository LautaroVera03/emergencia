package Ventanas;

import emergencia.EmpresaEmergencia;
import excepciones.PersonaEncontrada;
import excepciones.PersonaNoEncontrada;
import gestionEmpleados.Chofer;
import gestionEmpleados.Doctor;
import gestionEmpleados.Empleado;
import gestionEmpleados.EmpleadoAdministrativo;
import gestionEmpleados.Enfermero;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class VentanaEmpleados extends javax.swing.JDialog {
    private VentanaAdministrativo datosDeAdministrativo;
    private VentanaDoctor datosDoctor;
    private VentanaChofer datosChofer;
    private VentanaEnfermero datosEnfermero;
    private VentanaEliminarEmpleado eliminarEmpleado;
    private VentanaBuscarEmpleado buscarEmpleado;
    private ModificarEmpleado ventanaModificar;

    public VentanaEmpleados(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("EMPLEADOS");
        datosDeAdministrativo = new VentanaAdministrativo(parent,true);
        datosDoctor = new VentanaDoctor(parent,true);
        datosChofer = new VentanaChofer(parent,true);
        datosEnfermero = new VentanaEnfermero(parent,true);
        eliminarEmpleado = new VentanaEliminarEmpleado(parent,true);
        buscarEmpleado = new VentanaBuscarEmpleado(parent,true);
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jComboBoxOcupacion = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        btnGuardar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnAtras = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablaEmpleados = new javax.swing.JTable();
        txtSalario = new javax.swing.JTextField();
        txtDni = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jPanel2.setBackground(new java.awt.Color(102, 102, 102));

        jLabel1.setFont(new java.awt.Font("Rockwell", 0, 40)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("REGISTRO DE EMPLEADOS");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(183, 183, 183))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jLabel2.setFont(new java.awt.Font("Rockwell", 0, 20)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Datos del empleado:");

        jLabel3.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Apellido:");

        jLabel4.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nombre:");

        jLabel5.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Documento:");

        jLabel6.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Ocupacion:");

        jLabel7.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Salario:");

        jComboBoxOcupacion.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        jComboBoxOcupacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione una opción", "Administrativo", "Enfermero", "Doctor", "Chofer" }));

        jPanel3.setBackground(new java.awt.Color(102, 102, 102));

        btnGuardar.setBackground(new java.awt.Color(51, 51, 51));
        btnGuardar.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnEliminar.setBackground(new java.awt.Color(51, 51, 51));
        btnEliminar.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnEliminar.setForeground(new java.awt.Color(255, 255, 255));
        btnEliminar.setText("ELIMINAR");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnBuscar.setBackground(new java.awt.Color(51, 51, 51));
        btnBuscar.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnBuscar.setForeground(new java.awt.Color(255, 255, 255));
        btnBuscar.setText("BUSCAR");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnModificar.setBackground(new java.awt.Color(51, 51, 51));
        btnModificar.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(255, 255, 255));
        btnModificar.setText("MODIFICAR");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnLimpiar.setBackground(new java.awt.Color(51, 51, 51));
        btnLimpiar.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnLimpiar.setForeground(new java.awt.Color(255, 255, 255));
        btnLimpiar.setText("LIMPIAR");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnAtras.setBackground(new java.awt.Color(51, 51, 51));
        btnAtras.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnAtras.setForeground(new java.awt.Color(255, 255, 255));
        btnAtras.setText("ATRAS");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(btnGuardar)
                .addGap(18, 18, 18)
                .addComponent(btnEliminar)
                .addGap(18, 18, 18)
                .addComponent(btnBuscar)
                .addGap(18, 18, 18)
                .addComponent(btnModificar)
                .addGap(18, 18, 18)
                .addComponent(btnLimpiar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAtras)
                .addGap(33, 33, 33))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAtras, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jTablaEmpleados.setBackground(new java.awt.Color(255, 255, 255));
        jTablaEmpleados.setForeground(new java.awt.Color(0, 0, 0));
        jTablaEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTablaEmpleados);

        try {
            txtDni.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel4)
                                .addComponent(jLabel5)
                                .addComponent(jLabel3)
                                .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                                .addComponent(txtDni)))))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jComboBoxOcupacion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(txtSalario))
                .addGap(37, 37, 37)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxOcupacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 110, Short.MAX_VALUE)))
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        String nombre = txtNombre.getText();
        String apellido = txtApellido.getText();
        String dni = txtDni.getText();
        double salario;
        String ocupacion = jComboBoxOcupacion.getSelectedItem().toString();
        
        if(txtNombre.getText().isEmpty() || txtApellido.getText().isEmpty() || txtDni.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Por favor, complete todos los campos.", "EROR", JOptionPane.ERROR_MESSAGE);
        }

        try {
            salario = Double.parseDouble(txtSalario.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "El salario tiene que ser un numero valido.", "NUMERO INVALIDO", JOptionPane.ERROR_MESSAGE);
            txtSalario.setText("");
            return; 
        }

        if(txtSalario.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Por favor, complete todos los campos.", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        else{
            switch (ocupacion) {
                
                case "Administrativo":
                    datosDeAdministrativo.setVisible(true);
                    String cargo = datosDeAdministrativo.getCargo();
                    int idEmpleado = datosDeAdministrativo.getIdentificacion();
                    if(idEmpleado == 0){
                        break;
                    }
                    datosDeAdministrativo.limpiarCasilla();

                    Empleado administrativo = new EmpleadoAdministrativo(cargo, idEmpleado, nombre, apellido, dni, salario, ocupacion);

                    try {
                        EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(administrativo);
                        ordenarEmpleadosPorApellido();
                        JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                        limpiarCasillas();
                    } catch (PersonaEncontrada e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                    }
                    break;

                case "Doctor":
                    datosDoctor.setVisible(true);
                    String especialidad = datosDoctor.getEspecialidad();
                    int matricula = datosDoctor.getMatricula();
                    if(matricula == 0){
                        break;
                    }
                    datosDoctor.limpiarCasilla();

                    Empleado doctor = new Doctor(especialidad, matricula, nombre, apellido, dni, salario, ocupacion);

                    try {
                        EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(doctor);
                        ordenarEmpleadosPorApellido();
                        JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                        limpiarCasillas();
                    } catch (PersonaEncontrada e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                    }
                    break;

                case "Enfermero":
                    datosEnfermero.setVisible(true);
                    int idEnfermero = datosEnfermero.getIdentificacion();
                    if(idEnfermero == 0){
                        break;
                    }
                    datosEnfermero.limpiarCasilla();

                    Empleado enfermero = new Enfermero(idEnfermero, nombre, apellido, dni, salario, ocupacion);
                    try {
                        EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(enfermero);
                        ordenarEmpleadosPorApellido();
                        JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                        limpiarCasillas();
                    } catch (PersonaEncontrada e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                    }
                    break;

                case "Chofer":
                    datosChofer.setVisible(true);
                    int numLicencia = datosChofer.getNumLicencia();
                    if(numLicencia == 0){
                        break;
                    }
                    datosChofer.limpiarCasilla();

                    Empleado chofer = new Chofer(numLicencia, nombre, apellido, dni, salario, ocupacion);
                    try {
                        EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(chofer);
                        ordenarEmpleadosPorApellido();
                        JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                        limpiarCasillas();
                    } catch (PersonaEncontrada e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                    }
                    break;
                default:
                    JOptionPane.showMessageDialog(this, "Seleccione una ocupación.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        eliminarEmpleado.setVisible(true);
        eliminarEmpleado.limpiarCasillaDNI();
        
        if(eliminarEmpleado.isConfirmado()){
             String dniEmpleadoEliminar = eliminarEmpleado.getDniEmpleadoEliminar();
            try {
                EmpresaEmergencia.instancia().getRegistroEmpleados().eliminarEmpleado(dniEmpleadoEliminar);
                JOptionPane.showMessageDialog(this, "Se eliminó correctamente el empleado.");
                agregarEmpleadoATabla();
            } catch (PersonaNoEncontrada e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                eliminarEmpleado.limpiarCasillaDNI();
            } 
        }else{
            JOptionPane.showMessageDialog(this, "Se canceló la eliminación del empleado.");
            eliminarEmpleado.limpiarCasillaDNI();
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiarCasillas();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        buscarEmpleado.setVisible(true);
        buscarEmpleado.limpiarCasillaDNI();
        
        if(buscarEmpleado.isConfirmado()){
            String dniBuscar = buscarEmpleado.getDniBuscar();
            try{ 
                Empleado empleadoBuscado = EmpresaEmergencia.instancia().getRegistroEmpleados().buscarEmpleado(dniBuscar);
               
                if(empleadoBuscado.getOcupacion().equals("Administrativo")){
                    EmpleadoAdministrativo empleadoAux = (EmpleadoAdministrativo) empleadoBuscado;
                    JOptionPane.showMessageDialog(this, "Nombre: "+empleadoAux.getNombre()+"\nApellido: "+empleadoAux.getApellido()
                        +"\nDNI: "+empleadoAux.getDni()+"\nSalario: "+empleadoAux.getSalario()+"\nOcupacion: "+empleadoAux.getOcupacion()+
                            "\nCargo: "+empleadoAux.getCargo()+"\nID: "+empleadoAux.getIdEmpleado(),"Informacion del empleado",JOptionPane.INFORMATION_MESSAGE);
                }
                if(empleadoBuscado.getOcupacion().equals("Doctor")){
                    Doctor empleadoAux = (Doctor) empleadoBuscado;
                    JOptionPane.showMessageDialog(this, "Nombre: "+empleadoAux.getNombre()+"\nApellido: "+empleadoAux.getApellido()
                        +"\nDNI: "+empleadoAux.getDni()+"\nSalario: "+empleadoAux.getSalario()+"\nOcupacion: "+empleadoAux.getOcupacion()+
                            "\nEspecialidad: "+empleadoAux.getEspecialidad()+"\nMatricula: "+empleadoAux.getNumeroMatricula(),"Informacion del empleado",JOptionPane.INFORMATION_MESSAGE);
                }
                if(empleadoBuscado.getOcupacion().equals("Enfermero")){
                    Enfermero empleadoAux = (Enfermero) empleadoBuscado;
                    JOptionPane.showMessageDialog(this, "Nombre: "+empleadoAux.getNombre()+"\nApellido: "+empleadoAux.getApellido()
                        +"\nDNI: "+empleadoAux.getDni()+"\nSalario: "+empleadoAux.getSalario()+"\nOcupacion: "+empleadoAux.getOcupacion()+
                            "\nID: "+empleadoAux.getIdEnfermero(),"Informacion del empleado",JOptionPane.INFORMATION_MESSAGE);
                }
                if(empleadoBuscado.getOcupacion().equals("Chofer")){
                    Chofer empleadoAux = (Chofer) empleadoBuscado;
                    JOptionPane.showMessageDialog(this, "Nombre: "+empleadoAux.getNombre()+"\nApellido: "+empleadoAux.getApellido()
                        +"\nDNI: "+empleadoAux.getDni()+"\nSalario: "+empleadoAux.getSalario()+"\nOcupacion: "+empleadoAux.getOcupacion()+
                            "\nNumero de Licencia: "+empleadoAux.getNumLicencia(),"Informacion del empleado",JOptionPane.INFORMATION_MESSAGE);
                }    
            }catch(PersonaNoEncontrada e){
                JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                buscarEmpleado.limpiarCasillaDNI();
            }
            
        }else{
            JOptionPane.showMessageDialog(this, "Se canceló la búsqueda del empleado.");
            buscarEmpleado.limpiarCasillaDNI();
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        try {
            ModificarEmpleado ventanaEmpleado = new ModificarEmpleado(null, true);
        } catch (PersonaNoEncontrada ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());       
        }
    }//GEN-LAST:event_btnModificarActionPerformed
    
    public void cargarTabla(){
        agregarEmpleadoATabla();
    }
    public void agregarEmpleadoATabla() {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("Nombre");
        modelo.addColumn("Apellido");
        modelo.addColumn("Documento");
        modelo.addColumn("Salario");
        modelo.addColumn("Ocupacion");

        for (Empleado empleado : EmpresaEmergencia.instancia().getRegistroEmpleados().getEmpleados()) {
            Object[] rowData = {
                empleado.getNombre(),
                empleado.getApellido(),
                empleado.getDni(),
                empleado.getSalario(),
                empleado.getOcupacion()
            };
            modelo.addRow(rowData);
        }
        jTablaEmpleados.setModel(modelo);
        jTablaEmpleados.setEnabled(false);
    }
    public void ordenarEmpleadosPorApellido(){
        List<Empleado> listaEmpleados = EmpresaEmergencia.instancia().getRegistroEmpleados().getEmpleados();
        
        Comparator<Empleado> comparadorPorApellido = new Comparator<Empleado>(){
            @Override
            public int compare(Empleado empleado1, Empleado empleado2){
                return empleado1.getApellido().compareTo(empleado2.getApellido());
            }
        };
        Collections.sort(listaEmpleados, comparadorPorApellido);
        agregarEmpleadoATabla();
    }
    public void limpiarCasillas(){
        txtNombre.setText("");
        txtApellido.setText("");
        txtDni.setText("");
        txtSalario.setText("");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox<String> jComboBoxOcupacion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTablaEmpleados;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JFormattedTextField txtDni;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtSalario;
    // End of variables declaration//GEN-END:variables
}
