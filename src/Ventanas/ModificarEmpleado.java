/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package Ventanas;

import emergencia.EmpresaEmergencia;
import excepciones.PersonaEncontrada;
import excepciones.PersonaNoEncontrada;
import gestionEmpleados.Chofer;
import gestionEmpleados.Doctor;
import gestionEmpleados.Empleado;
import gestionEmpleados.EmpleadoAdministrativo;
import gestionEmpleados.Enfermero;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Owner
 */
public class ModificarEmpleado extends javax.swing.JDialog {
    private VentanaAdministrativo datosDeAdministrativo;
    private VentanaDoctor datosDoctor;
    private VentanaChofer datosChofer;
    private VentanaEnfermero datosEnfermero;
    private VentanaBuscarEmpleado buscarEmpleado;
    private Empleado empleadoAModificar;
    private VentanaEmpleados ventanaEmpleado;
    private String dniRecibido;
    
    public ModificarEmpleado(java.awt.Frame parent, boolean modal) throws PersonaNoEncontrada {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("MODIFICAR EMPLEADOS");
        limpiarCampos();
        datosDeAdministrativo = new VentanaAdministrativo(parent,true);
        datosDoctor = new VentanaDoctor(parent,true);
        datosChofer = new VentanaChofer(parent,true);
        datosEnfermero = new VentanaEnfermero(parent,true);
        buscarEmpleado = new VentanaBuscarEmpleado(parent,true);
        ventanaEmpleado = new VentanaEmpleados(parent, true);
        buscarEmpleado.setVisible(true);
        recibirDni(buscarEmpleado.getDniBuscar());
        dniRecibido = mandarDni();
        empleadoAModificar = EmpresaEmergencia.instancia().getRegistroEmpleados().buscarEmpleado(dniRecibido);
        cargarVentana(empleadoAModificar);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombreEmpleado = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtApellidoEmpleado = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtDniEmpleado = new javax.swing.JFormattedTextField();
        txtComboxOcupacionEmpleado = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtSalarioEmpleado = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setForeground(new java.awt.Color(0, 102, 102));

        jLabel2.setFont(new java.awt.Font("Rockwell", 0, 20)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Modificar empleado:");

        jLabel4.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nombre:");

        jLabel3.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Apellido:");

        jLabel5.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Documento:");

        try {
            txtDniEmpleado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        txtComboxOcupacionEmpleado.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        txtComboxOcupacionEmpleado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione una opción", "Administrativo", "Enfermero", "Doctor", "Chofer" }));

        jLabel6.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Ocupacion:");

        jLabel7.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Salario:");

        btnCancelar.setBackground(new java.awt.Color(51, 51, 51));
        btnCancelar.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnGuardar.setBackground(new java.awt.Color(51, 51, 51));
        btnGuardar.setFont(new java.awt.Font("Rockwell", 0, 12)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtApellidoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel3)
                                        .addComponent(txtNombreEmpleado)
                                        .addComponent(txtDniEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(27, 27, 27)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtComboxOcupacionEmpleado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(txtSalarioEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(btnGuardar)
                        .addGap(35, 35, 35)
                        .addComponent(btnCancelar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel2)
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombreEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtComboxOcupacionEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtApellidoEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSalarioEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDniEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public void cargarVentana (Empleado empleado){
    if (empleado != null) {
              txtNombreEmpleado.setText(empleadoAModificar.getNombre());
              txtApellidoEmpleado.setText(empleadoAModificar.getApellido());
              txtDniEmpleado.setText(empleadoAModificar.getDni());
              txtSalarioEmpleado.setText(String.valueOf(empleadoAModificar.getSalario()));
              txtComboxOcupacionEmpleado.setSelectedItem(empleadoAModificar.getOcupacion());
              setVisible(true);
      } else {
          JOptionPane.showMessageDialog(this, "Empleado no encontrado", "Error", JOptionPane.ERROR_MESSAGE);
          }
    }
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        setVisible(false);
        JOptionPane.showMessageDialog(this, "La modificación del empleado fue cancelada.");
    }//GEN-LAST:event_btnCancelarActionPerformed
    public void recibirDni(String dni){
        dniRecibido = dni;
    }
    public String mandarDni (){
        return dniRecibido;
    }
    public void limpiarCampos (){
        txtNombreEmpleado.setText("");
        txtApellidoEmpleado.setText("");
        txtDniEmpleado.setText("");
        txtSalarioEmpleado.setText("");
        txtComboxOcupacionEmpleado.setSelectedItem("Seleccione una opción");
    }
    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        String ocupacion = empleadoAModificar.getOcupacion();
        double salarioNuevo;
        String nombreNuevo;
        String apellidoNuevo;
        String dniNuevo;
        switch (ocupacion){
            case "Administrativo":
            EmpleadoAdministrativo empleadoAdministrativo = (EmpleadoAdministrativo) empleadoAModificar;
            datosDeAdministrativo.setTxtCargo(empleadoAdministrativo.getCargo());
            datosDeAdministrativo.setTxtFIdentificacion(empleadoAdministrativo.getIdEmpleado());
            datosDeAdministrativo.setVisible(true);
            String cargoNuevo = datosDeAdministrativo.getCargo();
            int IdNuevo = datosDeAdministrativo.getIdentificacion();
            nombreNuevo = txtNombreEmpleado.getText();
            apellidoNuevo = txtApellidoEmpleado.getText();
            dniNuevo = txtDniEmpleado.getText();
            try{
                salarioNuevo = Double.parseDouble(txtSalarioEmpleado.getText());
            
            }catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "El salario tiene que ser un numero valido.", "NUMERO INVALIDO", JOptionPane.ERROR_MESSAGE);
            txtSalarioEmpleado.setText("");
            return; 
             }
            if(IdNuevo == 0){
                break;
            }
            datosDeAdministrativo.limpiarCasilla();

            Empleado administrativo = new EmpleadoAdministrativo(cargoNuevo, IdNuevo, nombreNuevo, apellidoNuevo, dniNuevo, salarioNuevo, ocupacion);
            try {
                        EmpresaEmergencia.instancia().getRegistroEmpleados().eliminarEmpleado(dniRecibido);
                        EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(administrativo);

                        JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                    } catch (PersonaEncontrada e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                    }catch (PersonaNoEncontrada ex){
                        JOptionPane.showMessageDialog(this, ex.getMessage());

                    }
            limpiarCampos();
                    break;
            case "Doctor":
                Doctor empleadoDoctor = (Doctor) empleadoAModificar;
                datosDoctor.setTxtEspecialidad(empleadoDoctor.getEspecialidad());
                String matricula = String.valueOf(empleadoDoctor.getNumeroMatricula());
                datosDoctor.setTxtFMatricula(matricula);
                datosDoctor.setVisible(true);
                nombreNuevo = txtNombreEmpleado.getText();
                apellidoNuevo = txtApellidoEmpleado.getText();
                dniNuevo = txtDniEmpleado.getText();
                int matriculaNueva = datosDoctor.getMatricula();
                String especialidadNueva = datosDoctor.getEspecialidad();
                try{
                salarioNuevo = Double.parseDouble(txtSalarioEmpleado.getText());
            
                }catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "El salario tiene que ser un numero valido.", "NUMERO INVALIDO", JOptionPane.ERROR_MESSAGE);
                txtSalarioEmpleado.setText("");
                return; 
                 }
                if(matriculaNueva == 0){
                    break;
                }
                datosDoctor.limpiarCasilla();

                Empleado doctor = new Doctor(especialidadNueva, matriculaNueva, nombreNuevo, apellidoNuevo, dniNuevo, salarioNuevo, ocupacion);
                try {
                    EmpresaEmergencia.instancia().getRegistroEmpleados().eliminarEmpleado(dniRecibido);
                    EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(doctor);

                    JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                } catch (PersonaEncontrada e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }catch (PersonaNoEncontrada ex){
                    JOptionPane.showMessageDialog(this, ex.getMessage());

                }
                limpiarCampos();
                break;
                
                
            case "Enfermero":
                Enfermero empleadoEnfermero = (Enfermero) empleadoAModificar;
                String idEnfermero = String.valueOf(empleadoEnfermero.getIdEnfermero());
                datosEnfermero.setTxtFIdentificacion(idEnfermero);
                datosEnfermero.setVisible(true);
                nombreNuevo = txtNombreEmpleado.getText();
                apellidoNuevo = txtApellidoEmpleado.getText();
                dniNuevo = txtDniEmpleado.getText();
                int idNuevo = datosEnfermero.getIdentificacion();
                try{
                salarioNuevo = Double.parseDouble(txtSalarioEmpleado.getText());
            
                }catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "El salario tiene que ser un numero valido.", "NUMERO INVALIDO", JOptionPane.ERROR_MESSAGE);
                txtSalarioEmpleado.setText("");
                return; 
                 }
                if(idNuevo == 0){
                        break;
                    }
                    datosEnfermero.limpiarCasilla();

                Empleado enfermero = new Enfermero(idNuevo, nombreNuevo, apellidoNuevo, dniNuevo, salarioNuevo, ocupacion);
                try {
                    EmpresaEmergencia.instancia().getRegistroEmpleados().eliminarEmpleado(dniRecibido);
                    EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(enfermero);
                    JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                } catch (PersonaEncontrada e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }catch (PersonaNoEncontrada ex){
                    JOptionPane.showMessageDialog(this, ex.getMessage());

                }
                limpiarCampos();
                break;    
                
            case "Chofer":
                Chofer empleadoChofer = (Chofer) empleadoAModificar;
                String numLicencia = String.valueOf(empleadoChofer.getNumLicencia());
                datosChofer.setTxtFNumeroLicencia(numLicencia);
                datosChofer.setVisible(true);
                nombreNuevo = txtNombreEmpleado.getText();
                apellidoNuevo = txtApellidoEmpleado.getText();
                dniNuevo = txtDniEmpleado.getText();
                int licenciaNueva = datosChofer.getNumLicencia();
                try{
                salarioNuevo = Double.parseDouble(txtSalarioEmpleado.getText());
            
                }catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "El salario tiene que ser un numero valido.", "NUMERO INVALIDO", JOptionPane.ERROR_MESSAGE);
                txtSalarioEmpleado.setText("");
                return; 
                 }
                if(licenciaNueva == 0){
                    break;
                }
                datosChofer.limpiarCasilla();

                Empleado chofer = new Chofer(licenciaNueva, nombreNuevo, apellidoNuevo, dniNuevo, salarioNuevo, ocupacion);
                try {
                    EmpresaEmergencia.instancia().getRegistroEmpleados().eliminarEmpleado(dniRecibido);
                    EmpresaEmergencia.instancia().getRegistroEmpleados().agregarEmpleado(chofer);
                    JOptionPane.showMessageDialog(this, "Se regitro correctamente el empleado.");
                } catch (PersonaEncontrada e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }catch (PersonaNoEncontrada ex){
                    JOptionPane.showMessageDialog(this, ex.getMessage());

                }
                limpiarCampos();
                break;   
                
        }
        setVisible(false);
        ventanaEmpleado.cargarTabla();
        JOptionPane.showMessageDialog(this, "La modificación del empleado fue exitosa.");        
    }//GEN-LAST:event_btnGuardarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtApellidoEmpleado;
    private javax.swing.JComboBox<String> txtComboxOcupacionEmpleado;
    private javax.swing.JFormattedTextField txtDniEmpleado;
    private javax.swing.JTextField txtNombreEmpleado;
    private javax.swing.JTextField txtSalarioEmpleado;
    // End of variables declaration//GEN-END:variables
}
