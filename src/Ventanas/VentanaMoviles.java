
package Ventanas;

import excepciones.MovilEncontrado;
import excepciones.MovilNoEncontrado;
import gestionMoviles.Movil;
import gestionMoviles.RegistroMovil;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;




public class VentanaMoviles extends javax.swing.JFrame {
    private RegistroMovil registroMovil;
    private Movil movil;
    DefaultTableModel modeloTabla = new DefaultTableModel();
    private boolean modoModificacion;
    private int contadorAutos = 0;
    

   
    public VentanaMoviles(java.awt.Frame parent, boolean modal) {
        initComponents();
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("AgregarAfiliado");
        setResizable(false);
        registroMovil = new RegistroMovil();
        modoModificacion = false;
        agregarMovilATabla();
        txtPatenteMovil.setText("");
        txtMarcaMovil.setText("");
        txtModeloMovil.setText("");
        txtFechaMovil.setText("AAAA-MM-DD");
        agregarEventos();
        btnContador.setEnabled(false);
    }
    private void agregarEventos() {   
       txtPatenteABuscarMovil.addMouseListener(new MouseAdapter() {
            @Override
                public void mouseClicked(MouseEvent e) {
                    txtPatenteABuscarMovil.setText("");
                }
        }   );
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        btnEliminar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnAgregar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnMantenimientoMovil = new javax.swing.JCheckBox();
        btnLimpiar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();
        txtMarcaMovil = new javax.swing.JFormattedTextField();
        txtModeloMovil = new javax.swing.JFormattedTextField();
        txtFechaMovil = new javax.swing.JFormattedTextField();
        txtPatenteABuscarMovil = new javax.swing.JTextField();
        txtPatenteMovil = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btnContador = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTableMoviles = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel1.setFont(new java.awt.Font("Rockwell", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("MOVILES");

        jLabel2.setFont(new java.awt.Font("Rockwell", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("ADMINISTRACIÓN DE");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setBackground(new java.awt.Color(0, 102, 102));
        jLabel3.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 102));
        jLabel3.setText("MOVIL:");

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jLabel5.setForeground(new java.awt.Color(0, 102, 102));
        jLabel5.setText("Patente:");

        jLabel6.setForeground(new java.awt.Color(0, 102, 102));
        jLabel6.setText("Marca:");

        jLabel7.setForeground(new java.awt.Color(0, 102, 102));
        jLabel7.setText("Modelo:");

        jLabel8.setForeground(new java.awt.Color(0, 102, 102));
        jLabel8.setText("Fecha de fabricacion:");

        jLabel9.setForeground(new java.awt.Color(0, 102, 102));
        jLabel9.setText("Mantenimiento:");

        btnAgregar.setBackground(new java.awt.Color(0, 102, 102));
        btnAgregar.setForeground(new java.awt.Color(204, 204, 204));
        btnAgregar.setText("Guardar/Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnCerrar.setText("Volver");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        btnMantenimientoMovil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMantenimientoMovilActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        jLabel10.setBackground(new java.awt.Color(0, 102, 102));
        jLabel10.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 102, 102));
        jLabel10.setText("Agregar un movil nuevo:");

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        try {
            txtMarcaMovil.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("*****************")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            txtModeloMovil.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            txtFechaMovil.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-##-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(btnMantenimientoMovil)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(btnEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModificar)
                .addGap(0, 14, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(70, 70, 70)
                        .addComponent(jLabel6))
                    .addComponent(jLabel3)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtModeloMovil)
                            .addComponent(txtPatenteMovil, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE))
                        .addGap(33, 33, 33)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMarcaMovil)
                            .addComponent(txtFechaMovil)))
                    .addComponent(txtPatenteABuscarMovil, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPatenteABuscarMovil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEliminar)
                    .addComponent(btnBuscar)
                    .addComponent(btnModificar))
                .addGap(36, 36, 36)
                .addComponent(jLabel10)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMarcaMovil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPatenteMovil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtModeloMovil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFechaMovil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnMantenimientoMovil)
                    .addComponent(btnAgregar))
                .addGap(29, 29, 29)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLimpiar)
                    .addComponent(btnCerrar))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jLabel4.setBackground(new java.awt.Color(0, 102, 102));
        jLabel4.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Moviles disposibles:");

        btnContador.setBackground(new java.awt.Color(0, 0, 0));
        btnContador.setForeground(new java.awt.Color(255, 255, 255));
        btnContador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContadorActionPerformed(evt);
            }
        });

        JTableMoviles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(JTableMoviles);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(47, 47, 47)
                                .addComponent(jLabel1))
                            .addComponent(jLabel2)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4)
                        .addGap(29, 29, 29)
                        .addComponent(btnContador, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(28, 28, 28)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(btnContador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

 
    private void btnContadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContadorActionPerformed

    }//GEN-LAST:event_btnContadorActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        String movilCambiar = txtPatenteABuscarMovil.getText().toUpperCase();
        if (movilCambiar.isEmpty() || movilCambiar.equals("Ingrese la patente")) {
            JOptionPane.showMessageDialog(this, "Por favor, ingrese una patente válida para poder modificar el movil.", "ERROR", JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                Movil movilAModificar = registroMovil.buscarMovil(movilCambiar);

                txtPatenteMovil.setText(movilAModificar.getPatente().toUpperCase());
                txtMarcaMovil.setText(movilAModificar.getMarca().toUpperCase());
                txtModeloMovil.setText(movilAModificar.getModelo().toUpperCase());
                txtFechaMovil.setText(String.valueOf(movilAModificar.getAnioFabricacion()));
                btnMantenimientoMovil.setSelected(movilAModificar.getMantenimientoRealizado());

                registroMovil.eliminarMovil(movilCambiar);
                contadorAutos--;
                txtPatenteABuscarMovil.setText("Ingrese la patente...");
            } catch (MovilNoEncontrado e) {

                JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }}
        txtPatenteABuscarMovil.setText("Ingrese la patente...");
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiarCamposTexto();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnMantenimientoMovilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMantenimientoMovilActionPerformed

        boolean mantenimientoRealizado = btnMantenimientoMovil.isSelected();
        actualizarEstadoMantenimiento(mantenimientoRealizado);
    }//GEN-LAST:event_btnMantenimientoMovilActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed

        String patente = txtPatenteMovil.getText().toUpperCase();
        String modelo = txtModeloMovil.getText().toUpperCase();
        String marca = txtMarcaMovil.getText().toUpperCase();
        String anioFabricacionStr = txtFechaMovil.getText();
        boolean mantenimientoRealizado = btnMantenimientoMovil.isSelected();

        if (patente.isEmpty() || modelo.isEmpty() || marca.isEmpty() || anioFabricacionStr.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Por favor, complete todos los campos.", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                anioFabricacionStr = anioFabricacionStr.trim();
                LocalDate anioFabricacion = LocalDate.parse(anioFabricacionStr);

                movil = new Movil(patente, modelo, marca, anioFabricacion, mantenimientoRealizado);
                registroMovil.agregarMovil(movil);
                agregarMovilATabla();
                limpiarCamposTexto();
                contadorAutos++;
                btnContador.setText(String.valueOf(contadorAutos));
                JOptionPane.showMessageDialog(this, "Se agregó correctamente el móvil.");
            } catch (DateTimeParseException e) {
                JOptionPane.showMessageDialog(this, "Por favor, ingrese una fecha de fabricación válida.", "ERROR", JOptionPane.ERROR_MESSAGE);
            } catch (MovilEncontrado e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

        String patenteBuscar = txtPatenteABuscarMovil.getText().toUpperCase();
        if (patenteBuscar.isEmpty() || patenteBuscar.equals("Ingrese la patente")) {
            JOptionPane.showMessageDialog(this, "Por favor, ingrese una patente válida para buscar el movil.", "ERROR", JOptionPane.ERROR_MESSAGE);
        }

        try {
            Movil movilBuscado = registroMovil.buscarMovil(patenteBuscar);
            JOptionPane.showMessageDialog(this, "Patente: "+movilBuscado.getPatente()+"\nMarca: "+movilBuscado.getMarca()+ "\nModelo: "+movilBuscado.getModelo()+ "\nFecha de Fabricación: "+movilBuscado.getAnioFabricacion());
        } catch (MovilNoEncontrado e) {

            JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        txtPatenteABuscarMovil.setText("Ingrese la patente...");
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed

        String patenteEliminar = txtPatenteABuscarMovil.getText().toUpperCase();
        if (patenteEliminar.isEmpty() || patenteEliminar.equals("Ingrese la patente")) {
            JOptionPane.showMessageDialog(this, "Por favor, ingrese una patente válida para poder eliminar el movil.", "ERROR", JOptionPane.ERROR_MESSAGE);
        }else {
            try {
                registroMovil.eliminarMovil(patenteEliminar);
                txtPatenteABuscarMovil.setText("Ingrese la patente");
                agregarMovilATabla();
                contadorAutos--;
                btnContador.setText(String.valueOf(contadorAutos));
            JOptionPane.showMessageDialog(this, "Se eliminó correctamente el móvil.");
            } catch (MovilNoEncontrado ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                
            }
            txtPatenteABuscarMovil.setText("Ingrese la patente...");
           
        }
        modoModificacion = false;
        limpiarCamposTexto();
    }//GEN-LAST:event_btnEliminarActionPerformed
    
    private void actualizarEstadoMantenimiento(boolean mantenimientoRealizado) {
        int filaSeleccionada = JTableMoviles.getSelectedRow();
        if (filaSeleccionada != -1) {
            modeloTabla.setValueAt(mantenimientoRealizado, filaSeleccionada, 2);
        }
    }
    
    private void limpiarCamposTexto() {
    txtPatenteMovil.setText("");
    txtModeloMovil.setText("");
    txtMarcaMovil.setText("");
    txtFechaMovil.setText("");
    }
    public List<Movil> ordenarMovilesPorPatente() {
        List<Movil> listaMoviles = registroMovil.getListaMovil();
        Collections.sort(listaMoviles);
        return listaMoviles;
    }
    private void agregarMovilATabla() {
    DefaultTableModel modeloTabla = new DefaultTableModel();
    modeloTabla.addColumn("Patente");
    modeloTabla.addColumn("Modelo");
    modeloTabla.addColumn("Mantenimiento");
    List<Movil> movilesOrdenados = ordenarMovilesPorPatente();

    for (Movil movil : movilesOrdenados) {
        Object[] fila = {
            movil.getPatente(),
            movil.getModelo(), 
            movil.getMantenimientoRealizado()
        };
        modeloTabla.addRow(fila);
    }
    JTableMoviles.setModel(modeloTabla);
    JTableMoviles.setEnabled(false);
}
    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable JTableMoviles;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JTextField btnContador;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JCheckBox btnMantenimientoMovil;
    private javax.swing.JButton btnModificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JFormattedTextField txtFechaMovil;
    private javax.swing.JFormattedTextField txtMarcaMovil;
    private javax.swing.JFormattedTextField txtModeloMovil;
    private javax.swing.JTextField txtPatenteABuscarMovil;
    private javax.swing.JTextField txtPatenteMovil;
    // End of variables declaration//GEN-END:variables
}
