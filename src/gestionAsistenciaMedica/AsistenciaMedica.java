
package gestionAsistenciaMedica;

import gestionAfiliados.Afiliado;
import gestionAfiliados.Direccion;
import gestionEmpleados.Enfermero;
import gestionEmpleados.Doctor;
import gestionEmpleados.Chofer;
import gestionMoviles.Movil;
import java.time.LocalDateTime;
import java.util.Objects;

public class AsistenciaMedica {
    private Afiliado afiliado; 
    private Direccion direccion;
    private LocalDateTime fecha;
    private Diagnostico diagnostico;
    private Doctor doctor;
    private Enfermero enfermero;
    private Chofer conductorDesignado;
    private Movil ambulancia;
    private String gradoDeAtencion;
    
    
    public AsistenciaMedica(Afiliado afiliado, Direccion direccion, LocalDateTime fecha, Diagnostico diagnostico, Doctor doctor, Enfermero enfermero, Movil ambulancia, Chofer conductorDesignado, String gradoDeAtencion ){
        this.afiliado = afiliado;
        this.direccion = direccion;
        this.fecha = fecha;
        this.diagnostico = diagnostico;
        this.doctor = doctor;
        this.enfermero = enfermero;
        this.ambulancia = ambulancia;
        this.conductorDesignado = conductorDesignado;
        this.gradoDeAtencion = gradoDeAtencion;
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public Diagnostico getDiagnostico() {
        return diagnostico;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Enfermero getEnfermero() {
        return enfermero;
    }

    public Chofer getConductorDesignado() {
        return conductorDesignado;
    }

    public Movil getAmbulancia() {
        return ambulancia;
    }

    public String getGradoDeAtencion() {
        return gradoDeAtencion;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
         if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AsistenciaMedica solicitudAux = (AsistenciaMedica) obj;
        if (!Objects.equals(this.afiliado, solicitudAux.afiliado)) {
            return false;
        }
        return Objects.equals(this.fecha, solicitudAux.fecha);
    }
    
    @Override
    public String toString() {
        return "AsistenciaMedica{" + "afiliado=" + afiliado + ", direccion=" + direccion + ", fecha=" + fecha + ", diagnostico=" + diagnostico + ", doctor=" + doctor + ", enfermero=" + enfermero + ", conductorDesignado=" + conductorDesignado + ", ambulancia=" + ambulancia + ", tipoDeAtencion=" + gradoDeAtencion + '}';
    }
}