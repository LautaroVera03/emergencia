
package gestionAsistenciaMedica;

public class Diagnostico {
    private String tipo;
    private String estado;
    private String sintomas;
    private String intervenciones;
    private String conclusion;

    public Diagnostico(String tipo, String estado, String sintomas, String intervenciones, String conclusion) {
        this.tipo = tipo;
        this.estado = estado;
        this.sintomas = sintomas;
        this.intervenciones = intervenciones;
        this.conclusion = conclusion;
    }

    public String getTipo() {
        return tipo;
    }

    public String getEstado() {
        return estado;
    }

    public String getSintomas() {
        return sintomas;
    }

    public String getIntervenciones() {
        return intervenciones;
    }

    public String getConclusion() {
        return conclusion;
    }
    
    
}