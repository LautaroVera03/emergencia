package gestionAsistenciaMedica;

import excepciones.AsistenciaMedicaNoEncontrada;
import excepciones.AsistenciaMedicaEncontrada;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;

public class RegistroAsistenciasMedicas {
    private List<AsistenciaMedica> registrosAsistencias;
    
    public RegistroAsistenciasMedicas(){
        this.registrosAsistencias = new ArrayList<AsistenciaMedica>();
    }
    
    public void agregarAsistenciaMedica(AsistenciaMedica asistenciaMedica) throws AsistenciaMedicaEncontrada{
        if(registrosAsistencias.contains(asistenciaMedica)){
            throw new AsistenciaMedicaEncontrada("La solicitud de asistencia medica que intenta agregar ya se encuentra registrada.");
        }else {
            registrosAsistencias.add(asistenciaMedica);
        }
    }

    public void eliminarAsistenciaMedica(AsistenciaMedica asistenciaMedicaEliminar) throws AsistenciaMedicaNoEncontrada{
        if(registrosAsistencias.contains(asistenciaMedicaEliminar)){
            registrosAsistencias.remove(asistenciaMedicaEliminar);
        }else{
            throw new AsistenciaMedicaNoEncontrada("La solicitud de asistencia medica que intenta eliminar no se encuentra registrada.");
        }
    }

    public void modificarAsistenciaMedica(AsistenciaMedica asistenciaMedicaExistente, AsistenciaMedica nuevaAsistenciaMedica) throws AsistenciaMedicaNoEncontrada{
        if(registrosAsistencias.contains(asistenciaMedicaExistente)){
            int indice = registrosAsistencias.indexOf(asistenciaMedicaExistente);
            registrosAsistencias.set(indice, nuevaAsistenciaMedica);
        }
        else{
            try{
                agregarAsistenciaMedica(nuevaAsistenciaMedica);
            }catch(AsistenciaMedicaEncontrada e){
                System.out.println(e.getMessage());
            }
            throw new AsistenciaMedicaNoEncontrada("La solicitud de asistencia medica que intenta modificar no se encuentra registrada, por lo tanto se registrara. ");
        }
    }
     public List<AsistenciaMedica> buscarAsistenciaMedica(LocalDateTime fechaAsistencia) throws AsistenciaMedicaNoEncontrada {
       List<AsistenciaMedica> asistenciasMedicasEncontradas = new ArrayList();
        for (AsistenciaMedica asistenciaMedica : registrosAsistencias) {
            if (asistenciaMedica.getFecha().equals(fechaAsistencia)) {
                asistenciasMedicasEncontradas.add(asistenciaMedica);
            }
        }
        if(asistenciasMedicasEncontradas.isEmpty()){
                throw new AsistenciaMedicaNoEncontrada("No se encontraron solicitudes de asistencias medicas en la fecha establecida.");
        }
        return asistenciasMedicasEncontradas;       
    }
    
}
