package emergencia;

import gestionAfiliados.FicheroAfiliados;
import gestionAsistenciaMedica.RegistroAsistenciasMedicas;
import gestionDePagos.RegistroDePagos;
import gestionEmpleados.RegistroDeEmpleados;

public class EmpresaEmergencia {
    private static EmpresaEmergencia empresaEmergencia;
    private FicheroAfiliados ficheroAfiliados = new FicheroAfiliados();
    private RegistroAsistenciasMedicas registrosAsistencias = new RegistroAsistenciasMedicas();
    private RegistroDePagos registroDePagos = new RegistroDePagos();
    private RegistroDeEmpleados registroEmpleados = new RegistroDeEmpleados();
    
    private EmpresaEmergencia(){
        
    }
    public static EmpresaEmergencia instancia(){
        if(empresaEmergencia == null){
            
            empresaEmergencia = new EmpresaEmergencia();
        }
        return empresaEmergencia;
    }

    public FicheroAfiliados getFicheroAfiliados() {
        return ficheroAfiliados;
    }

    public RegistroAsistenciasMedicas getRegistrosAsistencias() {
        return registrosAsistencias;
    }

    public RegistroDePagos getRegistroDePagos() {
        return registroDePagos;
    }

    public RegistroDeEmpleados getRegistroEmpleados() {
        return registroEmpleados;
    }
    
}
