
package gestionDePagos;

import excepciones.FacturaNoEncontrada;
import gestionAfiliados.Afiliado;
import java.util.ArrayList;
import java.util.List;

public class RegistroDePagos {
    private List<Factura> facturas;
    
    
    public RegistroDePagos(){
        this.facturas = new ArrayList<>();
    }
    public void agregarFactura(Factura factura){
        if(!facturas.contains(factura)){
            facturas.add(factura);
        }
    }
    public Factura buscarFactura(int numeroFactura)throws FacturaNoEncontrada{
        for(Factura factura: facturas){
            if(factura.getNumeroFactura() == numeroFactura){
                return factura;
            }
        }
        throw new FacturaNoEncontrada("\nLa factura buscada con el numero: "+ numeroFactura +"no se encuentra dentro del registro.");
    }
    
   public List<Factura> buscarFacturasPorDNI(String dniAfiliado) {
    List<Factura> facturasAfiliado = new ArrayList<>();

    for (Factura factura : facturas) {
        Afiliado afiliadoAsociado = factura.getAfiliadoAsociado();

        // Verificar si el afiliado asociado es nulo antes de acceder a sus propiedades
        if (afiliadoAsociado != null && afiliadoAsociado.getDni().equals(dniAfiliado)) {
            facturasAfiliado.add(factura);
        }
    }

    return facturasAfiliado;
}

    
    public void eliminarFactura(Factura factura)throws FacturaNoEncontrada{
        if(facturas.contains(factura)){
            facturas.remove(factura);
        }else{
            throw new FacturaNoEncontrada("\nLa factura ya fue removida o no se encuentra dentro del registro.");
        }
    }
    public void modificarFactura(Factura facturaExistente, Factura nuevaFactura)throws FacturaNoEncontrada{
        if(facturas.contains(facturaExistente)){
            int indice = facturas.indexOf(facturaExistente);
            facturas.set(indice, nuevaFactura);
        }else{
            agregarFactura(nuevaFactura);
            throw new FacturaNoEncontrada("\nLa factura que desea modificar no se encuentra dentro del registro.");
        }
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }
    
}
