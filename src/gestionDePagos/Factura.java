
package gestionDePagos;

import excepciones.ItemNoEncontrado;
import gestionAfiliados.Afiliado;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Factura {
    private int numeroFactura;
    private LocalDate fecha;
    private Afiliado afiliadoAsociado;
    private List<Item> items; 

    public Factura(int numeroFactura, LocalDate fecha, Afiliado afiliadoAsociado) {
        this.numeroFactura = numeroFactura;
        this.fecha = fecha;
        this.afiliadoAsociado = afiliadoAsociado;
        this.items = new ArrayList<Item>();
    }
    public void agregarItem(Item item){
        items.add(item);
    }
    public void eliminarItem(Item itemBuscado)throws ItemNoEncontrado{
    try{
        Item itm = buscarItem(itemBuscado);
        items.remove(itm);
        }catch(ItemNoEncontrado e){
            throw new ItemNoEncontrado("El item no fue encontrado en la lista.");
        }
    }
    public Item buscarItem(Item item) throws ItemNoEncontrado{
        for(Item it: items){
            if(it.equals(item)){
                return it;
            }
        }
        throw new ItemNoEncontrado("El item no fue encontrado dentro de la lista.");
    }
    public void modificarItem(Item itemBuscado, Item item)throws ItemNoEncontrado{
        try {
            Item itm = buscarItem(itemBuscado);
            itm = item;
        }catch (ItemNoEncontrado e){
            System.out.println("Error...No fue encontrado el item." + e.getMessage());
        }
        
    }
    
    public List<Item> getItems (){
        return items;
    }

    public int getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(int numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Afiliado getAfiliadoAsociado() {
        return afiliadoAsociado;
    }

    public void setAfiliadoAsociado(Afiliado afiliadoAsociado) {
        this.afiliadoAsociado = afiliadoAsociado;
    }

    public double facturaTotal(){
        double total = 0;
        for (Item item : items){
            total += item.getTarifaAfiliado() + item.getTarifaFamiliar() + item.getAdicional();
        }
        return total;
    }
  
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Factura other = (Factura) obj;
        return this.numeroFactura == other.numeroFactura;
    }

}
