package gestionDePagos;

public class Item {
    private double tarifaAfiliado;
    private double tarifaFamiliar;
    private double adicional;

    public Item(double tarifaAfiliado, double tarifaFamiliar, double adicional){
        this.tarifaAfiliado = tarifaAfiliado;
        this.tarifaFamiliar = tarifaFamiliar;
        this.adicional = adicional;
    }

    public double getTarifaAfiliado() {
        return tarifaAfiliado;
    }

    public void setTarifaAfiliado(double tarifaAfiliado) {
        this.tarifaAfiliado = tarifaAfiliado;
    }

    public double getTarifaFamiliar() {
        return tarifaFamiliar;
    }

    public void setTarifaFamiliar(double tarifaFamiliar) {
        this.tarifaFamiliar = tarifaFamiliar;
    }

    public double getAdicional() {
        return adicional;
    }

    public void setAdicional(double adicional) {
        this.adicional = adicional;
    }
 
    
}
