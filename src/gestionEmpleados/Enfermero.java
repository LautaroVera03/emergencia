
package gestionEmpleados;


public class Enfermero extends Empleado{
    private int idEnfermero;

    public Enfermero(int idEnfermero, String nombre, String apellido, String dni, double salario, String ocupacion) {
        super(nombre, apellido, dni, salario, ocupacion);
        this.idEnfermero = idEnfermero;
    }

    public int getIdEnfermero() {
        return idEnfermero;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Enfermero other = (Enfermero) obj;
        return this.idEnfermero == other.idEnfermero;
    }
}
