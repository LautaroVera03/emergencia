
package gestionEmpleados;

public abstract class Empleado implements Comparable<Empleado>{
    private String nombre;
    private String apellido;
    private String dni;
    private double salario;
    private String ocupacion;
   

    public Empleado(String nombre, String apellido, String dni, double salario, String ocupacion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.salario = salario;
        this.ocupacion = ocupacion;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    @Override
    public int compareTo(Empleado otroEmpleado) {
        return this.getNombre().compareTo(otroEmpleado.getNombre());
    }
}
