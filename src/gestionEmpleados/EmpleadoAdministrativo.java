
package gestionEmpleados;

public class EmpleadoAdministrativo extends Empleado{
    private String cargo;
    private int idEmpleado;

    public EmpleadoAdministrativo(String cargo, int idEmpleado, String nombre, String apellido, String dni, double salario, String ocupacion) {
        super(nombre, apellido, dni, salario, ocupacion);
        this.cargo = cargo;
        this.idEmpleado = idEmpleado;
    }

    public String getCargo() {
        return cargo;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpleadoAdministrativo other = (EmpleadoAdministrativo) obj;
        return this.idEmpleado == other.idEmpleado;
    }
    
}
