
package gestionEmpleados;

public class Doctor extends Empleado{
    private String especialidad;
    private int numeroMatricula;

    public Doctor(String especialidad, int numeroMatricula, String nombre, String apellido, String dni, double salario, String ocupacion) {
        super(nombre, apellido, dni, salario, ocupacion);
        this.especialidad = especialidad;
        this.numeroMatricula = numeroMatricula;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public int getNumeroMatricula() {
        return numeroMatricula;
    }
   
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Doctor other = (Doctor) obj;
        return this.numeroMatricula == other.numeroMatricula;
    }
   
}
