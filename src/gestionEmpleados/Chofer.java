/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionEmpleados;

/**
 *
 * @author veral
 */
public class Chofer extends Empleado {
    private int numLicencia;

    public Chofer(int numLicencia, String nombre, String apellido, String dni, double salario, String ocupacion) {
        super(nombre, apellido, dni, salario, ocupacion);
        this.numLicencia = numLicencia;
    }

    public int getNumLicencia() {
        return numLicencia;
    }
     
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Chofer other = (Chofer) obj;
        return this.numLicencia == other.numLicencia;
    }

}
