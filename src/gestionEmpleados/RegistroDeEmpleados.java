
package gestionEmpleados;

import excepciones.PersonaEncontrada;
import excepciones.PersonaNoEncontrada;
import java.util.ArrayList;
import java.util.List;

public class RegistroDeEmpleados {
     private List<Empleado> empleados;
     
     public RegistroDeEmpleados(){
         this.empleados = new ArrayList<>();
     }
     
      public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }
    
     public void agregarEmpleado(Empleado empleado) throws PersonaEncontrada{
        if(empleados.contains(empleado)){
            throw new PersonaEncontrada("El empleado que desea agregar ya se encuentra registrado.");
        }else{
            empleados.add(empleado);
        }
    }

    public Empleado buscarEmpleado(String dni) throws PersonaNoEncontrada{
        for(Empleado empleado: empleados){
            if(empleado.getDni().equals(dni)){
                return empleado;
            }
        }
        throw new PersonaNoEncontrada("El empleado con DNI: "+ dni + " no fue encontrado.");       
    }
    
    public void eliminarEmpleado(String dni) throws PersonaNoEncontrada{
        Empleado empleadoAEliminar = buscarEmpleado(dni);
        if(empleados.contains(empleadoAEliminar)){
            empleados.remove(empleadoAEliminar);
        }else{
            throw new PersonaNoEncontrada("El empleado que desea eliminar no se encuentra registrado.");
        }
        
    }
     public void modificarEmpleado(Empleado empleadoExistente, Empleado nuevoEmpleado) throws PersonaNoEncontrada{
        if(empleados.contains(empleadoExistente)){
            int indice = empleados.indexOf(empleadoExistente);
            empleados.set(indice, nuevoEmpleado);
        }
        else{ 
            try{
                agregarEmpleado(nuevoEmpleado);
            }catch(PersonaEncontrada e){
                System.out.println(e.getMessage());
            }
            throw new PersonaNoEncontrada("El empleado no se encuentra dentro del registro de empleaado no puede ser modificado.\n");
        }
     }
     
}